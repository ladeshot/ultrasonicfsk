This project consists mainly of two programs.

The first is a Matlab script that outputs a .wav audio file to transmit FSK messages.
\Matlab\fskchallenge.m

The second is an Android application that can record FSK messages and decode them.
\src\ece\tamu\edu\FSK\FSK.java

Please look for instructions online if you need help running Matlab or Android programs.

Changes can be made to these programs such as alterring the frequency or bitrate of a transmission.
To do so, one must make the changes in both programs, so that they synchronize.
Here are a few example changes:

Bitrate:
	On line 7 of fskchallenge.m there is a variable called Pace.  This variable determines the bitrate.
	Higher numbers represent slower bitrates.  40 is very slow, but reliable.
	1 is fast, but may be susceptible to noise or distance.  These are the two extremes we've tested.
	The corresponding variable in FSK.java is on line 288 and is called var.
	
Frequencies:
	To change the high and low frequencies used in the FSK one can alter variables on
	line 10 of fskchallenge.m and line 292 of FSK.java.
	
If one would like to understand by reading the source code I recommend going over each
line of fskchallenge.m.  The second half of this program adds noise to the signal and decodes it.
It is significantly simpler than the Android application, but uses mostly the same techniques.	
	
Feel free to contact me at alecdeshotels@gmail.com if you have any questions that could not be solved by Google.


################################################################################
These programs were originally written by Mayank Manjrekar and Santosh Kumar.
They can be seen demonstrating them in this YouTube video:
https://www.youtube.com/watch?v=kb_e3DPPdsU
Anyone interested in identifying Luke's exact contributions can run a dif between this repo and the contents of original.zip.
This zip file contains the code originally provided to Luke by Mayank Manjrekar and Santosh Kumar

Luke Deshotels made modifications with permission to make the project use ultrasonic frequencies.
He also performed debugging and made the programs more suitable to experimentation.
Finally he added the GPL version 2 license upon release.

The white paper related to these programs was published by the Workshop on Offensive Technologies
held in association with Usenix Security 2014.  It is titled "Inaudible Sound as a Covert Channel in Mobile Devices".
#################################################################################