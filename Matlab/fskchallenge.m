%%
%% Android FSK Modem Challenge
%% Proof of Concept MATLAB Script
%%

% Setup sample frequency, symbol length (in samples), and symbol frequencies
pace = 40;
FS = 44100;
symlen = 128*pace;
symfreq = [18000 19000];
nsym = length(symfreq);

% Generate FSK waveforms as columns of symbols matrix
symbols = zeros(symlen,nsym);
index = (1:symlen)-1;
for i=1:nsym
  symbols(:,i) = cos(2*pi*symfreq(i)*index/FS)';
end

for i=1:nsym
  for j=1:symlen
      middle = symlen/2;
      distance = abs(j-middle);
      norm = distance/middle;
      flip = 1 - norm;
      symbols(j,i)=symbols(j,i)*flip;
  end
end

% Convert text to symbols
msgtxt = '0@1,2^3^4l5Q6n7C8f9W';
msgtxt = [msgtxt 0]; % mark EOF with hex 00
msgint = double(msgtxt);
symperbyte = ceil(log(256)/log(nsym));
msgdata = double(dec2base(msgint,nsym,symperbyte)')-48;
msgdata = msgdata(:)'+1;

% Setup preamble and sync word
prelen = 128;
synclen = 128;
preamble = [ones(1,prelen)];

% Choose pseudo-random sync word for repeatability

% sync = floor(rand(1,synclen)*nsym)+1;

sync=  [0,0,1,0,0,0,0,1,0,0,1,1,0,1,0,1,1,0,1,0,1,0,0,0,0,1,0,0,0,0,0,0,1,0,1,0,1,1,0,1,1,...
    1,1,0,1,0,0,1,1,1,1,0,0,1,0,0,0,0,1,1,1,1,1,0,0,1,0,0,1,0,1,0,0,0,1,1,0,0,1,0,1,0,...
    1,0,0,0,0,1,1,0,0,0,1,1,0,0,1,1,0,1,1,0,1,1,1,1,0,1,1,0,0,1,0,1,1,1,1,0,1,0,1,1,0,...
    0,0,0,0,1]+1;


% Modulate data symbols
data = [preamble sync msgdata];
x = symbols(:,data);
x = x(:)';

% Play sound
%x = 0.75*x;
% sound(x,FS);

% write wav file
wavwrite(x,FS,16,'fskDistance.wav');


% Add random time shift + noise
var = 1;
offset = 5*FS;
y = [zeros(1,symlen+randi(5)*FS) x zeros(1,symlen+randi(5)*FS)];
y = y + randn(1,length(y))*sqrt(var);
y = 0.1*y;
% y=x;
% Pause and record if necessary
if 0
  disp('recording...');
  r = audiorecorder(FS,16, 1);
  record(r);     % speak into microphone...
  pause;
  stop(r);
  %p = play(r);   % listen to complete recording
  y = double(getaudiodata(r,'int16'))/32768; % get data as int16 array
end

% Demod using matched filter for each waveform
z = zeros(nsym,length(y));
for i=1:nsym
  z(i,:) = filter(fliplr(symbols(:,i)'),1,y);
end

% Use non-coherent detection to avoid phase issues
pz = abs(z).^2;


% Do coarse search for tone-1 preamble
coarsewin = length(pz(1,:))-synclen*symlen-prelen*symlen;

% Fast sum of prelen adjacent outputs of tone-1 matched filter
pretest = pz(1,1:symlen:coarsewin);
premf = cumsum(pretest);
index = (prelen+1):length(premf);
premf = premf(index)-premf(index-prelen);

% Estimate location of premable and chop waveform before sync
[tmp,prelocsym] = max(premf);
prelocsam = (prelocsym+prelen-15)*symlen;
pz = pz(:,prelocsam:end);

% Search for sync pattern to get symbol timing
acqwin =length(pz(1,:))-synclen*symlen;
score = zeros(1,acqwin);

for j=1:acqwin
  % Subsample to synclen symbols starting at time j
  subz = pz(:,j:symlen:(j+symlen*synclen-1));

  % Sum energy bins associated with sync patten
  score(j) = sum(subz(sync+(1:nsym:(nsym*synclen))-1));
end

% Decode symbols
[tmp,t] = max(score);
pzt = pz(:,t:symlen:end);
[tmp,dec] = max(pzt);

% Reconstruct message
decmsgdata = dec((synclen+1):end);
k = floor(length(decmsgdata)/symperbyte);
tmp = reshape(char(decmsgdata(1:(symperbyte*k))+47),8,[])';
decmsgtxt = char(base2dec(tmp,nsym))'
eof = find(decmsgtxt==0);
decmsgtxt = decmsgtxt(1:(eof-1));

% Max message length in case of errors
if (length(decmsgtxt)>255)
 decmsgtxt = decmsgtxt(1:255);
end
